if __name__ == '__main__':
    n = 100
    a1 = 1
    a2 = 1
    a3 = 2

    for i in range(2, n):
        a3 = a1 + a2
        a1 = a2
        a2 = a3
    print(a3 if n > 2 else 1)
